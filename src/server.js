const express = require("express");
const port = 3000;
const app = express();

app.get("/", (req, res) => res.send("welcome"));

app.get("/print", (req, res) => {
    const message = req.query.a;
    res.send(message);
})

if (process.env.NODE_ENV === "test") {
    module.exports = app;
}

if (!module.parent) {
    app.listen(port, () => console.log(`Server running at localhost:${port}`))
}