const chai = require("chai");
const expect = chai.expect;
const request = require("request");
const app = require("../src/server");
const port = 3000;

let server;

const arequest = async (value) => new Promise((resolve, reject) => {
    request(value, (error, response) => {
        if(error) reject(error)
        else resolve(response)
    });
});

describe("Test REST API", () => {
    beforeEach("Start server", async () => {
        server=app.listen(port);
    });
    describe("Test functionality", () => {
        it("GET /print?a=viesti returns 'viesti'", async () => {
            const options = {
                method: 'GET',
                url: 'http://localhost:3000/print',
                qs: {a: 'viesti'}
            };
            await arequest(options).then((res) => {
                console.log({message: res.body});
                expect(res.body).to.equal('viesti');
            }).catch((res) => {
                console.log({res});
                expect(true).to.equal(false, 'print function failed');
            });
        });
    });
    afterEach(() => {
        server.close();
    });
});